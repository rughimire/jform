import os
from flask import send_from_directory


current_dir_path = os.path.abspath(__file__)
current_template_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "templates")
current_static_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "static")


def send_static_jforms(filename):
    return send_from_directory(current_static_path, filename)


def finalize_form(form_obj, kwargs):
    meta_data = kwargs.get('meta', None)
    if meta_data is None: return form_obj
    if 'meta' not in form_obj: form_obj['meta'] = {}
    for k, v in meta_data.items():
        form_obj['meta'][k] = v
    # print(form_obj)
    form_obj['meta']['id'] = form_obj['meta'].get('id', 'jform')
    # form_obj['meta']['is_modal'] = form_obj['meta'].get('is_modal', '')
    return form_obj


class JForms():
    def __init__(self, app):
        self.app = app
        self.add_jforms_template()

    def add_jforms_template(self):
        if self.app.jinja_loader:
            self.app.jinja_loader.searchpath.append(current_template_path)

        self.app.add_url_rule('/jforms-static/<path:filename>', endpoint='jforms-static',
                 view_func=send_static_jforms)