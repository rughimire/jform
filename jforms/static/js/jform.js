function MultiForm(fid = 'wtf') {
    this.currentTab = 0;
    this.totalTabs = document.getElementsByClassName("jforms-tab").length;
    this.nextButton = document.getElementById("nextBtn");
    this.nextButtonContents = document.getElementById("nextBtn").innerHTML;
    this.inputTypes = "input, textarea, datalist, select";

    this.showTab = function (n) {
        // This function will display the specified tab of the form ...
        let x = document.getElementsByClassName("jforms-tab");
        x[n].style.display = "block";
        x[n].classList.remove("jforms-unvisited-tab");
        if(x[n].classList.contains("invalid")){
            this.reportFieldValidity();
        }
        // ... and fix the Previous/Next buttons:
        let prev_button = document.getElementById("prevBtn");
        if (n == 0) {
            prev_button.classList.add("inactive-button");
            prev_button.disabled = true;
        } else {
            prev_button.classList.remove("inactive-button");
            prev_button.disabled = false;
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1) && !this.incompleteTabsLeft()) {
            this.changeArrowToSubmit();
        }
        else {
            this.changeSubmitToArrow();
        }
        // ... and run a function that displays the correct step indicator:
        this.fixStepIndicator(n);
        this.currentTab = n;
    };

    this.nextPrev = function (n, gotoDirectIndex) {
        //gotoDirectIndex is optional field, if given, skip directly to the index provided (used in step buttons)
        // This function will figure out which tab to display
        let x = document.getElementsByClassName("jforms-tab");
        // Exit the function if any field in the current tab is invalid:
        if (!this.validateForm() && n == 1) return false;
        // Hide the current tab:
        x[this.currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        if (gotoDirectIndex !== undefined) {
            this.currentTab = gotoDirectIndex;
        } else {
            this.currentTab += n;
        }
        // if you have reached the end of the form... :
        if (this.currentTab >= x.length) {
            // the currentTab can always only be greater by 1
            // if currentTab > x.length, then decrement by 1 to keep staying on the last tab instead of switching to a
            // tab that does not exist
            this.currentTab--;
            //If the end of form is reached, clicking next redirects to the first invalid input
            //if all fields are valid then submit the form
            let first_incomplete_tab = document.querySelectorAll(".jforms-tab.invalid, .jforms-unvisited-tab")[0];
            if (first_incomplete_tab === undefined || first_incomplete_tab.length == 0) {
                return false;
            } else {
                let switch_to_tab_id = first_incomplete_tab.id.split("-").pop();
                this.showTab(switch_to_tab_id);
            }
            //...the form gets submitted:
            //document.getElementById("regForm").submit();
        }
        // Otherwise, display the correct tab:
        this.showTab(this.currentTab);
    };

    this.validateForm = function () {
        // This function deals with validation of the form fields
        this.currentTab = parseInt(this.currentTab);
        let x, y, i, valid = true;
        x = document.getElementsByClassName("jforms-tab");
        y = x[this.currentTab].querySelectorAll(this.inputTypes);
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value === "" && y[i].required) {
                // add an "invalid" class to the tab and step:
                document.getElementsByClassName("jforms-tab")[this.currentTab].className += " invalid";
                document.getElementsByClassName("jforms-step")[this.currentTab].className += " invalid";
                // and set the current valid status to false:
                valid = false;
            }
        this.reportFieldValidity();
        }


        // If the valid status is true, mark the step as finished and valid and remove invalid if exists:
        if (valid) {
            document.getElementsByClassName("jforms-step")[this.currentTab].classList.remove("invalid");
            document.getElementsByClassName("jforms-tab")[this.currentTab].classList.remove("invalid");
            document.getElementsByClassName("jforms-step")[this.currentTab].className += " finish";
        }


        return valid; // return the valid status
    };

    this.fixStepIndicator = function (n) {
        // This function removes the "active" class of all steps...
        let i, x = document.getElementsByClassName("jforms-step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class to the current step:
        x[n].className += " active";
    };

    this.changeArrowToSubmit = function () {
        this.nextButton.innerHTML = "Submit";
        this.nextButton.classList.add("pl-2");

    };

    this.changeSubmitToArrow = function () {
        this.nextButton.classList.remove("pl-2");
        document.getElementById("nextBtn").innerHTML = this.nextButtonContents;
    };

    this.incompleteTabsLeft = function () {
        let first_incomplete_tab = document.querySelectorAll(".jforms-tab.invalid, .jforms-unvisited-tab")[0];
        if (first_incomplete_tab === undefined || first_incomplete_tab.length == 0) {
            return false;
        } else {
            let incomplete_tab_id = parseInt(first_incomplete_tab.id.split("-").pop());
            if (incomplete_tab_id === this.totalTabs - 1) {
                return false;
            }
            return first_incomplete_tab;
        }
    };

    this.reportFieldValidity = function () {
        let currentTabID = "jforms-tab-" + this.currentTab;
        let currentTabElement = document.getElementById(currentTabID);
        let currentTabChildInputs = currentTabElement.querySelectorAll(this.inputTypes);

        currentTabChildInputs.forEach(input => input.reportValidity());

        $(".jforms-datepicker").datepicker("hide");
    };

    this.showTab(this.currentTab);
}
