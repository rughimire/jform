from jforms import finalize_form


def simpleform(**kwargs): 
    form = {
        'meta': {
            'id' : 'jform_test',
            'title' : 'Form shown in the modal dialog',
         },
        'fields': [
            {
                'name'    : 'firstname',
                'label'   : 'पहिलो नाम',
                'css_class' : 'np',
                'attribs' : 'required',
                'input'   : {
                    'placeholder' : '',
                    'value'       : kwargs.get('firstname', ''),
                }
            },
            {
                'columns':[
                    {
                        'name'    : 'middlename',
                        'label'   : 'बिचको नाम',
                        'css_class' : 'np',
                        'input'   : {
                            'placeholder' : '',
                            'value'       : kwargs.get('middlename', ''),
                        }
                    },
                    {
                        'name'    : 'lastname',
                        'label'   : 'अन्तिम नाम',
                        'css_class' : 'np',
                        'input'   : {
                            'placeholder' : '',
                            'value'       : kwargs.get('lastname', ''),
                        }
                    },
                ]
            }
        ]
    }
    return finalize_form(form, kwargs)


def multiform(**kwargs):
    form = [
        [
            [{
                'name': "name",
                'attribs': "autofocus",
                'input': {
                    'placeholder': "Name",
                    'value': kwargs.get('default_name') or '',
                }
            }],
            [{
                'name': "period",
                'attribs': "required onchange='onChangePeriod();'",
                'select': {
                    'options': [('Anually', 'Anually'), ('Monthly', 'Monthly'), ],
                    'selected': [""],
                }
            },
                {
                'name': "period1",
                'attribs': "required onchange='onChangePeriod();'",
                'select': {
                    'options': [('Anually', 'Anually'), ('Monthly', 'Monthly'), ],
                    'selected': [""],
                }
            }
            ]
        ],
        [
            [{
                'name': "period",
                'attribs': "required onchange='onChangePeriod();'",
                'select': {
                    'options': [('Anually', 'Anually'), ('Monthly', 'Monthly'), ],
                    'selected': [""],
                }
            },
                {
                'name': "period1",
                'attribs': "required onchange='onChangePeriod();'",
                'select': {
                    'options': [('Anually', 'Anually'), ('Monthly', 'Monthly'), ],
                    'selected': [""],
                }
            }
            ]
        ],
        [[{'name': "TAB3", 'attribs': "required autofocus", 'input': {'placeholder': "TAB3", 'value':  ''}}], [
            {'name': "period1", 'attribs': "required onchange='onChangePeriod();'",
             'select': {'options': [('Anually', 'Anually'), ('Monthly', 'Monthly'), ], 'selected': [""], }}]],
        [[{'name': "LAST", 'attribs': "required autofocus", 'input': {'placeholder': "last", 'value': '', }}], [
            {'name': "Last", 'attribs': "required onchange='onChangePeriod();'",
             'select': {'options': [('Anually', 'Anually'), ('Monthly', 'Monthly'), ], 'selected': [""], }},
            {'name': "Final", 'attribs': "required onchange='onChangePeriod();'",
                'select': {'options': [('Anually', 'Anually'), ('Monthly', 'Monthly'), ], 'selected': [""], }}]],
    ]
    return form
