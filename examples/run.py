#!/usr/bin/env python3
import flask
import form
from jforms import JForms


app = flask.Flask(__name__)
# Add JForms templating to current Flask's instance
JForms(app)


@app.route('/vendors/<path:filepath>')
def serve_vendor(filepath):
    return flask.send_from_directory('vendors', filepath)


@app.route('/webfonts/<path:filepath>')
def serve_webfonts(filepath):
    return flask.send_from_directory('vendors/webfonts', filepath)


@app.route('/static/<path:filepath>')
def serve_static(filepath):
    return flask.send_from_directory('static', filepath)


@app.route('/')
def index():
    default_data = {
        'meta': {
            'url'       : '/',
            'method'    : 'POST',
        },
        'firstname' : 'Ram',
    }
    return flask.render_template("index.jinja2", forms=form.simpleform(**default_data))


@app.route('/', methods=['POST'])
def index_post():
    return "Form posted"


@app.route('/js')
def js():
    default_data = {
        'meta': {
            'on_load'   : 'on_load()',
            'on_submit'   : 'on_submit()',
            'include_js' : ['demo_form.js'],
        },
        'firstname' : 'defaut name',
    }
    return flask.render_template("index.jinja2", forms=form.simpleform(**default_data))


@app.route('/ajax')
def ajax():
    return flask.render_template('modal-form.jinja2')


@app.route('/modal_form')
def modal_form():
    default_data = {
        'meta': {
            'is_modal'    : True,
            'on_submit'   : 'on_submit()',
            'include_js'  : ['demo_form.js'],
        },
    }
    return flask.render_template("form-container.jinja2", forms=form.simpleform(**default_data))


@app.route('/multiform')
def multiform():
    return flask.render_template("multiform.jinja2", forms=form.multiform(default_name="user"))


if __name__ == '__main__':
    app.config["TEMPLATES_AUTO_RELOAD"] = True
    app.run(debug=True, host='0.0.0.0', port=8080, threaded=True, use_reloader=True)
