# jforms

jforms is a JSON-to-Form library for the Jinja 2 templating engine.

## Installation

Instruction installations for Jforms.

```bash
$ git clone https://gitlab.com/rhoit/jinja-form.git
$ cd jinja-form
$ pip install .
```

Install from git repo remote
```bash
$ pip install --upgrade git+https://gitlab.com/rughimire/jform.git
```


## Quickstart

You can start using Jforms with flask in 3 easy steps.

#### Step1: Import jforms 

```python
import flask
from jforms import JForms
```
#### Step2: Inject Jform's templates to Flask app's jinja instance 

```python
app = flask.Flask(__name__)
JForms(app)
```
#### Optional: Customizing default form
We can customize the jform behaviour be setting some global setting parameters

* `CUSTOM_ID_PREFIX` : prefix to be used on form element's id
* `CUSTOM_FIELD_STYLE`: css class on rhw form element's
* `CUSTOM_SELECT_STYLE`
* `CUSTOM_FILE_STYLE`
* `SUBMIT_BUTTON_STYLE`
* `RESET_BUTTON_STYLE`
* `CLOSE_BUTTON_STYLE`

Setting this from `flask` application object
```python
app.jinja_env.globals['DEFAULT_ID_PREFIX'] = ''
app.jinja_env.globals['CUSTOM_FIELD_STYLE'] = 'form-control-sm'
app.jinja_env.globals['CUSTOM_SELECT_STYLE'] = 'custom-select custom-select-sm'
app.jinja_env.globals['CUSTOM_FILE_STYLE'] = 'custom-file-input custom-file-input-sm'

app.jinja_env.globals['SUBMIT_BUTTON_STYLE'] = 'btn-sm btn-primary'
app.jinja_env.globals['RESET_BUTTON_STYLE'] = 'btn-sm btn-secondary'
app.jinja_env.globals['CLOSE_BUTTON_STYLE'] = 'btn-sm btn-secondary'
```
#### Step3: Use Jforms as you like!
```python
@app.route('/users')
def view_userLst():
    return flask.render_template(
        'jform.djhtml',
        forms  = jforms.users.User(**{
            'uname'  : 'admin',
            'groups' : [0],
        })
    )
```

## jfrom Demo : Flask demo app
Check out the examples in examples/ folder for examples.

Run within the jform
```bash
$ pip install -e .
$ python examples/run.py
```
Now you can try running the url http://localhost:8080/

## Typical jform structure
 ```json
 form = {
    'meta':{ },
    'fields': [ ]
 }
```
* meta: this section contains the meta data of the forms
* fields: this section will contains the fields to be displayed in the form

#### Meta
* `title` : Title of the form used for the modal dialog
* `is_modal` : `True` for modal dialog form
* `id` : if of the form on DOM
* `method` : method : `GET` | `POST` default is `POST`
* `url` : submission URL
* `on_submit` : javascript function to be called before submitting the function: it shuld return `bool`
* `on_load` : javascript function to be called once DOM is rendered
* `submit_label` : Label of the submit form
* `form_css` : css class to be applied to `<form>` tag
* `container_css` : css class to be applied to `<div>` which wraps the `<form>`
* `include_js` : list of the js files which will be rendered 
* `include_css` : list of the css files which will be rendered 
* `icon` : fontawesome icon name to be displayed when `is_modal` is `True`


`include_js` and `include_css` should be  accessible via `/static/css` or /`static/js`

Example meta section
```json
'meta':{
    'title':'This is test Form',
    'id':'test_form',               # 
    'method':'POST',                # method : GET | POST default
    'url':'url',
    'on_submit':'submit_form()',
    'on_load':'load_form()',
    'submit_label':'Save',
    'form_css':'',          # css class for the form
    'container_css':'',     # css class for the container div
    'include_js':[],        # list of js files to include within form
    'include_css':[],       # list of css files to include within form
}
```

#### Fields for Single/Standalone form generation
The fields section will contain the array of the json which will be rendered as element. The render of the element can be of 
* single element on one row
    ```
    'fields': [
        {
            'name'    : 'firstname',
            'label'   : 'पहिलो नाम',
            'css_class' : 'np',
            'attribs' : 'required',
            'input'   : {
                'placeholder' : '',
                'value'       : kwargs.get('firstname', ''),
            }
        },
    ]
    ```
* muti element on one row
    ```
    'fields': [
        {
            'columns':[
                {
                    'name'    : 'middlename',
                    'label'   : 'बिचको नाम',
                    'css_class' : 'np',
                    'input'   : {
                        'placeholder' : '',
                        'value'       : kwargs.get('middlename', ''),
                    }
                },
                {
                    'name'    : 'lastname',
                    'label'   : 'अन्तिम नाम',
                    'css_class' : 'np',
                    'input'   : {
                        'placeholder' : '',
                        'value'       : kwargs.get('lastname', ''),
                    }
                },
            ],
        },
    ]
    ```
#### Enabling Javascript
We need to define two jinja sections on the base template
* `jform_scripts`
* `jform_styles`




# Contribution
## conventions  :-(
* Use `space` over `tab`
* Line break
    * Single line break on logic change
        ```python
        # code block

        if SOMETHING:
            # DO
        else:
            # DO

        # code block
        ```
    * Double line break on function and class change
        ```python
        def one_func():
            # code here

        
        def two_func():
            # code here
        ```
* always leave one blank line at the end of the file
