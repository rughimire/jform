from setuptools import find_packages, setup


# Reference: Serious Python, Chapter 5.3
#Excellent answer on how to diagnose packaging problems: https://stackoverflow.com/questions/50585246/pip-install-creates-only-the-dist-info-not-the-package
setup(setup_requires=['pbr'], pbr=True,
      name="jforms",
      packages=["jforms"],
      package_data={"": ['jforms/templates/*']}
)
